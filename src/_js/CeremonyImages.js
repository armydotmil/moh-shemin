function CeremonyImages() {
    this.index = 0;
    this.images = $('#ceremony .ceremony_image_btn');
    this.len = this.images.length;
    this.img = null;
    this.alt_text = null;
    this.title = null;
    this.description = null;
    this.lightBoxImage = null;
    this.originalImage = null;
}

/**
 * Set up the click listner on the images in
 * the ceremony images section.
 */
CeremonyImages.prototype.onClick = function() {
    var context = this;
    this.images.click(function() {
        context.img = $(this).find('img');
        context.alt_text = context.img.attr('alt');
        context.p_text = $(this).find('p').text();
        context.index = context.images.index($(this));

        // retrieve ceremony_image.html
        $.get('ceremony_image.html', function(data) {
            $('html').css('overflow-y', 'hidden');

            $('#page').append(data);

            $('#lightbox').focus();

            context.title = $('.lightbox_image h2');
            context.description = $('.lightbox_image p:first');
            context.lightBoxImage = $('.lightbox_image img');
            context.originalImage = $('.lightbox_image .download a');

            context.setDisplay(context.images.eq(context.index));

            // on click outside lightbox
            $('#lightbox_outter_content').click(function(e) {
                e.stopPropagation();
            });

            // on close
            $('#lightbox,#button_icon_close').click(function() {
                $('#lightbox').remove();
                $('html').css('overflow-y', 'scroll');

                $('.lightbox_image p:first').html('');

                $('.lightbox_image h2').text('');

                context.index = 0;
            });

            // on click right
            $('.lightbox_button_wrap_right .bottom_lightbox_buttons').click(function() {

                if (context.index < context.len - 1) {
                    context.index++;
                }

                context.setDisplay(context.images.eq(context.index));
            });

            // on click left
            $('.lightbox_button_wrap_left .bottom_lightbox_buttons').click(function() {
                if (context.index > 0) {
                    context.index--;
                }

                context.setDisplay(context.images.eq(context.index));
            });
        });
    });
};

/**
 * Update the lightbox with the
 * appropiate content
 * @param {element} elm
 */
CeremonyImages.prototype.setDisplay = function(elm) {
    
    if(this.index === 0){
        $('.lightbox_button_wrap_left .bottom_lightbox_buttons').hide();
    } else if(this.index === this.len - 1){
        $('.lightbox_button_wrap_right .bottom_lightbox_buttons').hide();
    } else {
        $('.lightbox_button_wrap_left .bottom_lightbox_buttons').show();
        $('.lightbox_button_wrap_right .bottom_lightbox_buttons').show();
    }
    
    this.title.text('');
    this.description.text('');
    this.img = $(elm).find('img');
    this.alt_text = this.img.attr('alt');
    this.p_text = $(elm).find('p').text();
    this.description.text(this.alt_text);
    this.title.text(this.p_text);
    var newImage = new Image(),
        context = this;

    newImage.onload = function() {
        context.lightBoxImage.attr('src', this.src.replace('_460', '_660'));
    };
    newImage.src = this.img.attr('src');

    this.originalImage.attr('href', this.img.attr('src').replace('_460', ''));
};
